package org.lang.linkedlist;
import java.lang.Throwable;

public class LinkedList {
	private ListNode first;
	private ListNode last;
	public LinkedList(int data) {
		this.first = new ListNode(data);
		this.last = this.first;
	}
	public void push(int data) {
		ListNode nextNode = new ListNode(data);
		last.setNext(nextNode);
		last = last.getNext();
	}
	public ListNode getFirst() {
		return first;
	}
	public ListNode getLast() {
		return last;
	}
	public void printList() {
		ListNode current = this.first;
		while (current!=null) {
			System.out.println(current.getData());
			current = current.getNext();
		}
	}
	public ListNode getNode(int n) {
		throw new UnsupportedOperationException("getNode(n) not implemented");
	}
	public ListNode find(int n) {
		throw new UnsupportedOperationException("find(n) not implemented");
	}
}
